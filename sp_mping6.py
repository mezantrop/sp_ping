#!/usr/bin/env python3

# Copyright (c) 2018 Mikhail Zakharov <zmey20000@yahoo.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# Usage:
#   sp_mping6.py -i interface [-a address] [-m ttl] [-t timeout] [-v]
#
# Options:
#   -i interface    Interface to work on or lo0
#   [-a address]    IPv6 multicast address to ping or ff02::1
#   [-m ttl]		TTL of outgoing packets. Default is 64
#   [-s number]     Packet sequence number: 0-65535. Default is 0
#   [-t timeout]	Timeout on socket. Default is 5
#   [-v]			Be verbose (show more info about packets)
#
# Note! You must be root to run this program.

# 2018.08.10	v1.0		Mikhail Zakharov <zmey20000@yahoo.com>
#       Initial release
# 2018.08.11	v1.0.1		Mikhail Zakharov <zmey20000@yahoo.com>
#       Minor code cleaning
# 2018.09.08	v1.0.2		Mikhail Zakharov <zmey20000@yahoo.com>
#       Take ICMP sequence number from reply packet


import socket
import struct
import time
import os
import sys
import signal
import getopt


# Defaults can be overridden by getopt() below
target = 'ff02::1'                          # Default address to ping
interface = None                            # Network interface name must be specified in opts
timeout = 5                                 # Socket timeout in seconds
seq = 0                                     # Sequence number or 0
ttl = 64                                    # TTL
verbose = False                             # Make less noise


def usage(ecode=1, etext=''):
    if etext:
        print('{}\n'.format(etext), file=sys.stderr)

    print('Locate all neighbour IPv6 hosts sending ping request to a multicast IPv6 address\n'
          '\n'
          'Usage:\n'
          '\tsp_mping6.py -i interface [-a address] [-m ttl] [-t timeout] [-v]\n'
          '\n'
          'Options:\n'
          '\t-i interface\tInterface to work on\n'
          '\t[-a address]\tIPv6 multicast address to ping or ff02::1\n'
          '\t[-m ttl]\tTTL of outgoing packets. Default is 64\n'
          '\t[-s number]\tPacket sequence number: 0-65535. Default is 0\n'
          '\t[-t timeout]\tTimeout on socket. Default is 5\n'
          '\t[-v]\t\tBe verbose (show more info about packets)\n'
          '\n'
          'Note! You must be root to run this program.\n'
          )

    exit(ecode)


def sig_handler(sig, frame):
    if verbose:
        print('\nDone')
    exit(2)


def clk_chksum(icmp_packet):
    """Calculate ICMP packet checksum"""

    packet_len = len(icmp_packet)
    summ = 0
    for i in range(0, packet_len, 2):
        if i + 1 < packet_len:
            # Fold 2 neighbour bytes into a number and add it to the summ
            summ += icmp_packet[i] + (icmp_packet[i + 1] << 8)
        else:
            # If there is an odd number of bytes, fake the second byte
            summ += icmp_packet[i] + 0
    # Add carry bit to the sum
    summ = (summ >> 16) + (summ & 0xffff)
    # Truncate to 16 bits and return the checksum
    return ~summ & 0xffff


def mping6(target_host, sock, sequence=0, ttl=64):
    """Ping target: send ECHO_REQUEST and get ECHO_RESPONSE"""

    # Packet header definition
    iphdr_len = 0
    icmphdr_len = 8             # ICMP header length is 8 bytes
    icmp_type_request = 128     # ICMP IPv6 ECHO_REQUEST
    icmp_type_reply = 129       # ICMP IPv6 ECHO_REPLY

    icmp_code = 0
    icmp_checksum = 0
    icmp_id = os.getpid() & 0xffff  # Generate ID field using PID converted to 16 bit
    # Some ICMP payload examples. Do not make them too long:
    icmp_data = b'\x50\x49\x4E\x47\x2D\x50\x4F\x4E\x47\x20\x46\x52\x4F\x4D' \
                b'\x20\x5A\x4D\x45\x59\x32\x30\x30\x30\x30\x40\x59\x41\x48' \
                b'\x4F\x4F\x2E\x43\x4F\x4D'
    # icmp_data = b'12345678' + b'1234567890' * 4

    data_len = len(icmp_data)

    send_timestamp = time.time()    # Packet creation time
    out_packet = struct.pack('BBHHHQ{}s'.format(data_len), icmp_type_request, icmp_code,
                             icmp_checksum, icmp_id, sequence, int(send_timestamp), icmp_data)
    icmp_checksum = clk_chksum(out_packet)
    out_packet = struct.pack('BBHHHQ{}s'.format(data_len), icmp_type_request, icmp_code,
                             icmp_checksum, icmp_id, sequence, int(send_timestamp), icmp_data)


    # getaddrinfo returns an array of tuples (ainfo) for each address family and socket_kind.
    # We can check AddressFamily.name in ainfo[0] for desired address family AF_INET|AF_INET6 and
    # fetch sockaddr in ainfo[4] to submit it in sendto().
    # sockaddr format IPv6 - (address, port, flow info, scope id)
    target_address = None
    # Specify any portnumber (1 for example) in getaddrinfo() just to please the socket library
    try:
        for ainfo in socket.getaddrinfo(target_host, 1):
            if ainfo[0].name == 'AF_INET6':
                target_address = ainfo[4]
                break
    except socket.gaierror:
        print('Fatal: Unable to get address for:', target_host, file=sys.stderr)
        return 2

    # Send ICMP packet to the target_address formed by sockaddr structure above
    try:
        sock.sendto(out_packet, target_address)
    except socket.error:
        etype, evalue, etrb = sys.exc_info()
        print(evalue.args[1], file=sys.stderr)
        return 2

    if verbose:
        print('Ping: {pkt}, {host}'.format(pkt=struct.unpack('BBHHHQ', out_packet[:-data_len]), host=target_host))

    while True:
        # Let the buffer_size be the packet maximum size we expect
        buffer_size = 40 + icmphdr_len + struct.calcsize('Q') + data_len     # calcsize('Q'): timestamp size

        reply = None
        host = None
        try:
            reply, host = sock.recvfrom(buffer_size)
        except socket.timeout:                                              # Nothing found. Are all hosts down?
            if verbose:
                print('No answer. All hosts are down?')
            return 1
        except:
            print('Fatal: General error in recvfrom()', file=sys.stderr)
            return 2

        recv_timestamp = time.time()

        # Actual IP packet size of the reply
        packet_size = icmphdr_len + struct.calcsize("Q") + data_len
        in_packet = struct.unpack('BBHHH', reply[0:icmphdr_len])

        if in_packet[0] == icmp_type_reply and in_packet[3] == icmp_id:
            # This is Echo Reply packet (icmp_type_reply in in_packet[0]) to our request with ID (in_packet[3])
            if verbose:
                print('Pong: {pkt}, {host}, {size}, {time:0.4f}'.format(
                    pkt=in_packet[:-1], host=host[0], size=packet_size,
                    time=(recv_timestamp - send_timestamp) * 1000))
            else:
                print('{size} bytes from {host}: seq={seq} ttl={ttl} time={time:0.4f} ms'.format(
                    size=packet_size, host=host[0], seq=in_packet[4], ttl=ttl,
                    time=(recv_timestamp - send_timestamp) * 1000))
        else:
            if recv_timestamp - send_timestamp > timeout:
                return 0


# ----------------------------------------------------------------------------------------------------------------------
signal.signal(signal.SIGINT, sig_handler)

opts = None
try:
    opts, args = getopt.getopt(sys.argv[1:], 'a:i:m:s:t:v')
except getopt.GetoptError as err:
    usage(2, str(err))

for opt, arg in opts:
    try:
        if opt == '-a':                                 # [-a address]      IPv6 multicast address to ping or ff02::1
            target = arg
        elif opt == '-i':                               # -i interface      interface to work on
            interface = arg
        elif opt == '-m':                               # -m ttl
            ttl = int(arg)
        elif opt == '-s':                               # -s number         packet sequence number or 0
            seq = int(arg)
        elif opt == '-t':                               # -t timeout
            timeout = int(arg)
        elif opt == '-v':                               # -v verbose
            verbose = True
        else:
            pass                                        # Unknown options are detected by getopt() exception above
    except ValueError:
        usage(2, 'Fatal: Wrong argument: "{}" to: "{}" option is specified'.format(arg, opt))

if not interface:
    usage(2, 'Fatal: You must specify network interface to work on')

s = None
try:
    # Sorry guys, non-privileged ICMP does not work on most of OS so:
    s = socket.socket(socket.AF_INET6, socket.SOCK_RAW, socket.getprotobyname('ipv6-icmp'))
except PermissionError:
    print('Fatal: You must be root to send ICMP packets', file=sys.stderr)
    exit(2)
except:
    print('Fatal: General error in socket()', file=sys.stderr)
    exit(2)

s.settimeout(timeout)
s.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_UNICAST_HOPS, ttl)

# Main ping-pong function
result = mping6(target + '%' + interface, s, sequence=seq, ttl=ttl)

s.close()
