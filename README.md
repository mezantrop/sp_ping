# Simple ping implementation in Python using sockets

<a href="https://www.buymeacoffee.com/mezantrop" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>


## sp_ping.py - Send ICMP ECHO_REQUEST packets to network hosts

```
Send ICMP ECHO_REQUEST packets to network hosts. Simple ping implementation in Python

Usage:
	sp_ping.py -h host [-s source][-6][-c count][-i interval][-m ttl][-t timeout][-ov]

Options:
	-h host		    Target host to ping
	[-s source]	    Source address
	[-6]		    Use IPv6 protocol instead of IPv4
	[-c count]	    Send count only packets. Default is 0xffffff
	[-i interval]	Interval between two requests in seconds. Default is 1
	[-m ttl]	    TTL of outgoing packets. Default is 64
	[-t timeout]	Timeout on socket. Default is 5
	[-o]		    Send one packet only (conditional ping) to get host Up/Down status
	[-v]		    Be verbose (show more info about packets)

Note! You must be root to run this program.
  ```

## sp_mping6.py - Locate all neighbour IPv6 hosts sending ping request to a multicast IPv6 address

```
Locate all neighbour IPv6 hosts sending ping request to a multicast IPv6 address

Usage:
	sp_mping6.py -i interface [-a target] [-S source]
		[-s sequence] [-m ttl] [-t timeout] [-v]

Options:
	-i interface	Interface to work on
	[-a target]	    IPv6 multicast address to ping or ff02::1
	[-S source]	    IPv6 source address
	[-m ttl]	    TTL of outgoing packets. Default is 64
	[-s number]	    Packet sequence number: 0-65535. Default is 0
	[-t timeout]	Timeout on socket. Default is 5
	[-v]		    Be verbose (show more info about packets)

Note! You must be root to run this program.
```

## mping6.awk - Awk script to quickly find alive IPv6 addresses on the local network

```
Usage:
  mping6.awk [count=n] [interval=m] interface

Options:
  interface	Network interface to serach for IPv6 hosts on
  [count=n]	Send count only pings. Default is 2
  [interval=m]	Interval between pings. Default is 10
```
